var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server) // socket.io
  , sanitizer = require('sanitizer'); // sanitize inputs

var users = []; // array of all online users

server.listen(8080);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

// sends a json list of all online users and the room their in
app.get('/users', function(req, res){
  res.send(users);
});

app.get('/stats', function(req, res){
  var usersOnline = users.length;
  res.send({numberOnline: usersOnline});
});


// sends out json dictonary of all active rooms
app.get('/rooms', function(req, res){
  // res.send(io.sockets.manager.rooms);
  var keys = [];
  for(var key in io.sockets.manager.rooms){
    if(key){
            keys.push(key.slice(1));
    }
  }
  res.send(keys);
});

app.get('/jquery.js', function (req, res) {
  res.sendfile(__dirname + '/jquery.js');
});

io.sockets.on('connection', function (socket) {

  socket.on('add user',function(data){

   if (data.username) { // if the username is not empty

        // store the username in the socket session (sanitze first!)
        socket.username = sanitizer.escape(data.username); 
        // add the room to the socket session (once again, we sanitize!)
        socket.room = sanitizer.escape(data.room);
        // the socket joins that room
        socket.join(socket.room);
        // add the user to the users array
        users.push(data);

        // send out an event to all clients in that room alerting of the new user
        io.sockets.in(socket.room).emit('newUser', {username: socket.username});

     };
   });

  socket.on('msgSent', function (data) {
    console.log(data);
    var cleanString = sanitizer.escape(data.msg);
    if (socket.username) {
      if(cleanString){
          // io.sockets.emit('newMsg',{msg: cleanString, username: socket.username});
          io.sockets.in(socket.room).emit('newMsg', {msg: cleanString, username: socket.username});
      }
    };
  });

  socket.on('disconnect', function(socket){
      // alert everyone in the rooms that the user has disconected
      io.sockets.in(socket.room).emit('newMsg', {msg: socket.username + ' has disconnected', username: 'SERVER'});
      // find the disconnectd user in the array
      var i = users.indexOf(socket);
      // earase that user from out users array
      users.splice(i, 1);
    
      
  });

});