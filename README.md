# README #


### QuickChat ###

QuickChat is a simple web app that allows for realtime cross-browser chat rooms.

### Setup ###

* will add a package.json soon but for now just:

* Clone this repository 
```sh
node app.js
```
### Features ###

* cross browser support
* HTTP API

### API ###
QuickChat has an API for a third party applications.

* GET /users - Returns a json list of users online
* GET /stats - Returns the amount of users online
* GET /rooms - Returns a json list of users online and what room the user is in

### Dependencies ###
* socket.io
* express
* node